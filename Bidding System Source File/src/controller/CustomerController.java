package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import database.DatabaseConnection;
import model.bidding;
import model.customer;

public class CustomerController {

	
	// Find Customer
	public boolean findCustomer(customer Customer) throws Exception{
		
		boolean found = false;
		
		DatabaseConnection dbcon = new DatabaseConnection();
		Connection con = dbcon.getConnection();
		
		String sql = "select fName from customer where fName = ?";
		PreparedStatement prepare = con.prepareStatement(sql);
		prepare.setString(1, Customer.getfName());
		ResultSet rs = prepare.executeQuery();
		
		if(rs.next()) {
			found = true;
		}
		
		con.close();
		return found;
	}
	
	
	// Adding Customer
	public int addCustomer(customer Customer) throws Exception{
		
		int success = -1;
		
		DatabaseConnection dbcon = new DatabaseConnection();
		Connection con = dbcon.getConnection();
		
		String sql = "insert into customer (fName, noTel, password, email) values (?,?,?,?)";
		
		if(findCustomer(Customer)  == false) {
			
			PreparedStatement prepare = con.prepareStatement(sql);
			prepare.setString(1,Customer.getfName());
			prepare.setLong(2, Customer.getNoTel());
			prepare.setString(3, Customer.getPassword());
			prepare.setString(4, Customer.getEmail());
			success = prepare.executeUpdate();
		}
		
		con.close();
		return success;
	}
	
	// List Down all the Customer
	public ArrayList<customer> getCustomers() throws Exception {
		
		ArrayList<customer> customers = new ArrayList<customer>();
		
		DatabaseConnection dbcon = new DatabaseConnection();
		Connection con = dbcon.getConnection();
		
		String sql = "select fname, email, noTel from customer";
		PreparedStatement prepare = con.prepareStatement(sql);
		
		ResultSet rs = prepare.executeQuery();
		
		while(rs.next()) {
			
			customer Customer = new customer();
			Customer.setfName(rs.getString(1));
			Customer.setEmail(rs.getString(2));
			Customer.setNoTel(rs.getLong(3));
			customers.add(Customer);
		}
		
		con.close();
		return customers;
	}
	
	
	// Update Customer
	public void updateCustomer(customer Customer) throws Exception{
		
		
		
		DatabaseConnection dbcon = new DatabaseConnection();
		Connection con = dbcon.getConnection();
		
		String sql = "update customer set noTel = ?, email = ?, password = ? where fName =  ?";
		
		//if(findCustomer(Customer) == true) {
			
			PreparedStatement prepare = con.prepareStatement(sql);
			prepare.setLong(1, Customer.getNoTel());
			prepare.setString(2, Customer.getEmail());
			prepare.setString(3, Customer.getPassword());
			prepare.setString(4, Customer.getfName());
			prepare.executeUpdate();
		//}

		System.out.println("Done");
		con.close();
		
		
	}
	
	// Delete Customer 
	public void  deleteCustomer(customer Customer) throws Exception {
		
		DatabaseConnection dbcon = new DatabaseConnection();
		Connection con = dbcon.getConnection();
		
		String sql = "delete from customer where fName = ?";
		
		PreparedStatement prepare = con.prepareStatement(sql);
		prepare.setString(1, Customer.getfName());
		prepare.executeUpdate();
		
		System.out.println("Done");
		con.close();
	}
	
	public ArrayList<bidding> getBidCustomer(String name) throws Exception {
		
		ArrayList<bidding> products = new ArrayList<bidding>();
		
		DatabaseConnection dbcon = new DatabaseConnection();
		Connection con = dbcon.getConnection();
		
		//String sql = "select name,year,type,cond from product where name = ?";
		String sql = "SELECT * FROM prod_bid AS pd JOIN bidding AS b ON (pd.id_bid = b.id) JOIN product AS p ON (pd.id_product = p.id) where cust_Name = ?";
		PreparedStatement prepare = con.prepareStatement(sql);
		prepare.setString(1, name);
		ResultSet rs = prepare.executeQuery();
		
		while(rs.next()) {
			
			bidding Bidding = new bidding();
			Bidding.setName(rs.getString(13));
			//Bidding.setYear(rs.getInt(2));
			//Bidding.setType(rs.getString(3));
			//Bidding.setCondition(rs.getString(4));
			Bidding.setIntialPrice(rs.getString(8));
			Bidding.setFinalPrice(rs.getString(9));
			//Bidding.setStartTime(rs.getString(7));
			//Bidding.setEndTime(rs.getString(8));
			//Bidding.setName(rs.getString(10));
			//Bidding.setYear(rs.getInt(11));
			//Bidding.setType(rs.getString(12));
			//Bidding.setCondition(rs.getString(13));
			Bidding.setStatus(rs.getString(6));
			products.add(Bidding);
			//customers.add(Customer);
		}
		
		con.close();
		return products;
		
		
	}
	
	/*
	 * public static void main(String[] args) {
	 * 
	 * CustomerController customerController = new CustomerController();
	 * 
	 * try { for(customer Customer: customerController.getCustomers()) {
	 * 
	 * System.out.println(Customer.getfName());
	 * System.out.println(Customer.getEmail());
	 * System.out.println(Customer.getNoTel());
	 * 
	 * } } catch (Exception e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); } }
	 */
	
	/*
	 * public static void main(String[] args) {
	 * 
	 * CustomerController customerController = new CustomerController(); try {
	 * 
	 * customer Customer = new customer(); Customer.setfName("Customer001");
	 * Customer.setNoTel(012445674); Customer.setPassword("customer1234");
	 * Customer.setEmail("customer001@utem.com");
	 * //System.out.println(dealerController.addDealer(Dealer));
	 * System.out.println(customerController.addCustomer(Customer));
	 * System.out.println("Data Updated");
	 * 
	 * } catch (Exception e) { e.printStackTrace(); } }
	 */
	
	/*
	 * public static void main(String[] args) {
	 * 
	 * CustomerController customerController = new CustomerController();
	 * 
	 * 
	 * try { customer Customer = new customer(); Customer.setfName("Ain");
	 * Customer.setNoTel(9999); Customer.setEmail("Changed");
	 * 
	 * //customerController.updateCustomer(Customer);
	 * customerController.deleteCustomer(Customer); } catch (Exception e) { // TODO:
	 * handle exception } }
	 */
}


