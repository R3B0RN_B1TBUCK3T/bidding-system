package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
// import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import database.DatabaseConnection;
import model.bidding;
import model.customer;
import model.dealer;



public class BiddingController {
	
	//Find Bidding
	public boolean findBid(bidding Bidding) throws Exception{
		
		boolean found = false;
		
		DatabaseConnection dbcon = new DatabaseConnection();
		Connection con = dbcon.getConnection();
		
		String sql = "select name from product where name = ?";
		PreparedStatement prepare = con.prepareStatement(sql);
		prepare.setString(1, Bidding.getName());
		ResultSet rs = prepare.executeQuery();
		
		if(rs.next()) {
			found = true;
		}
		
		con.close();
		return found;
	}
	
	// Adding Bidding
	public int addBid(bidding Bidding, String name, String cust_name) throws Exception {
		
		int success = -1;
		
		String id_bid = null ;
		String id_product = null;
		
		DatabaseConnection dbcon = new DatabaseConnection();
		Connection con = dbcon.getConnection();
		
		String sql_2 = "insert into bidding (intialPrice, finalPrice, startTime, endTime) values (?,?,?,?)";
		String sql_1 = "insert into product (id,name,year,type,cond) values (?,?,?,?,?)";
		String sql_3 = "insert into prod_bid (id_bid, id_product,cust_Name,dealer_name,status) values (?,?,?,?,?)";
		
		// Select ID
		String sql_4 = "SELECT MAX(ID) AS LastID FROM bidding";
		String sql_5 = "SELECT MAX(ID) AS LastID FROM product";
		
		
		
		if(findBid(Bidding) == false ) {
			
			PreparedStatement prepare = con.prepareStatement(sql_1);
			prepare.setInt(1, Bidding.getId());
			prepare.setString(2, Bidding.getName());
			prepare.setInt(3, Bidding.getYear());
			prepare.setString(4, Bidding.getType());
			prepare.setString(5, Bidding.getCondition());
			prepare.executeUpdate();
			
			PreparedStatement prepare_02 = con.prepareStatement(sql_2);
			prepare_02.setString(1, Bidding.getIntialPrice());
			prepare_02.setString(2, Bidding.getFinalPrice());
			prepare_02.setString(3, Bidding.getStartTime());
			prepare_02.setString(4, Bidding.getEndTime());
			success = prepare_02.executeUpdate();
			
			
			PreparedStatement prepare_03 = con.prepareStatement(sql_4);
			ResultSet rs_01 = prepare_03.executeQuery();
			
			if(rs_01.next()) {
				
				id_bid = rs_01.getString(1);
			}
			
			
			PreparedStatement prepare_04 = con.prepareStatement(sql_5);
			ResultSet rs_02 = prepare_04.executeQuery();
			
			if(rs_02.next()) {
				
				id_product = rs_02.getString(1);
			}
			
			
			PreparedStatement prepare_05 = con.prepareStatement(sql_3);
			prepare_05.setString(1, id_bid);
			prepare_05.setString(2, id_product);
			prepare_05.setString(3, cust_name);
			prepare_05.setString(4, name);
			prepare_05.setString(5, "In process");
			success = prepare_05.executeUpdate();
			
		}
		
		con.close();
		return success;
	}
	
public int addBidAdmin(bidding Bidding) throws Exception {
		
		int success = -1;
		
		String id_bid = null ;
		String id_product = null;
		
		DatabaseConnection dbcon = new DatabaseConnection();
		Connection con = dbcon.getConnection();
		
		String sql_2 = "insert into bidding (intialPrice, finalPrice, startTime, endTime) values (?,?,?,?)";
		String sql_1 = "insert into product (name,year,type,cond) values (?,?,?,?)";
		String sql_3 = "insert into prod_bid (id_bid, id_product,status) values (?,?,?)";
		
		// Select ID
		String sql_4 = "SELECT MAX(ID) AS LastID FROM bidding";
		String sql_5 = "SELECT MAX(ID) AS LastID FROM product";
		
		
		
		if(findBid(Bidding) == false ) {
			
			PreparedStatement prepare = con.prepareStatement(sql_1);
			prepare.setString(1, Bidding.getName());
			prepare.setInt(2, Bidding.getYear());
			prepare.setString(3, Bidding.getType());
			prepare.setString(4, Bidding.getCondition());
			prepare.executeUpdate();
			
			PreparedStatement prepare_02 = con.prepareStatement(sql_2);
			prepare_02.setString(1, Bidding.getIntialPrice());
			prepare_02.setString(2, Bidding.getFinalPrice());
			prepare_02.setString(3, Bidding.getStartTime());
			prepare_02.setString(4, Bidding.getEndTime());
			success = prepare_02.executeUpdate();
			
			
			PreparedStatement prepare_03 = con.prepareStatement(sql_4);
			ResultSet rs_01 = prepare_03.executeQuery();
			
			if(rs_01.next()) {
				
				id_bid = rs_01.getString(1);
			}
			
			
			PreparedStatement prepare_04 = con.prepareStatement(sql_5);
			ResultSet rs_02 = prepare_04.executeQuery();
			
			if(rs_02.next()) {
				
				id_product = rs_02.getString(1);
			}
			
			
			PreparedStatement prepare_05 = con.prepareStatement(sql_3);
			prepare_05.setString(1, id_bid);
			prepare_05.setString(2, id_product);
			prepare_05.setString(3, "In process");
			success = prepare_05.executeUpdate();
			
		}
		
		con.close();
		return success;
	}
	
	// List Down all the Product
	public ArrayList<bidding> getBid(String name) throws Exception {
		
		ArrayList<bidding> products = new ArrayList<bidding>();
		
		DatabaseConnection dbcon = new DatabaseConnection();
		Connection con = dbcon.getConnection();
		
		//String sql = "select name,year,type,cond from product where name = ?";
		String sql = "SELECT * FROM prod_bid AS pd JOIN bidding AS b ON (pd.id_bid = b.id) JOIN product AS p ON (pd.id_product = p.id) WHERE name = ?";
		PreparedStatement prepare = con.prepareStatement(sql);
		prepare.setString(1, name);
		ResultSet rs = prepare.executeQuery();
		
		while(rs.next()) {
			
			bidding Bidding = new bidding();
//			Bidding.setName(rs.getString(1));
//			Bidding.setYear(rs.getInt(2));
//			Bidding.setType(rs.getString(3));
//			Bidding.setCondition(rs.getString(4));
			Bidding.setIntialPrice(rs.getString(8));
			Bidding.setFinalPrice(rs.getString(9));
			Bidding.setStartTime(rs.getString(10));
			Bidding.setEndTime(rs.getString(11));
			Bidding.setName(rs.getString(13));
			Bidding.setYear(rs.getInt(14));
			Bidding.setType(rs.getString(15));
			Bidding.setCondition(rs.getString(16));
			products.add(Bidding);
			//customers.add(Customer);
		}
		
		con.close();
		return products;
		
		
	}
	
	public boolean startBid(bidding Bidding, double bidPrice, String name) throws Exception{
		
		boolean execute = false;
		double finalPrice = Double.parseDouble(Bidding.getFinalPrice());
		double intialPrice = Double.parseDouble(Bidding.getIntialPrice());
		
		// To identify whether user bid more than actual price
		boolean statement_1 = bidPrice >= intialPrice;
		
		// To identify whether user bid amount more than final amount
		boolean statement_2 = bidPrice >= finalPrice;
		
		if(statement_1) {
			
			if(statement_2) {
				
				finalPrice = bidPrice;
				System.out.println("Bid Price\t: " + bidPrice);
				System.out.println("Final Price\t: " + finalPrice);
				
				DatabaseConnection dbcon = new DatabaseConnection();
				Connection con = dbcon.getConnection();
				
				// UPDATE prod_bid AS pb JOIN bidding AS b ON (pb.id_bid = b.id) JOIN product AS p ON (pb.id_product = p.id) SET b.finalPrice = ? WHERE p.name = ?
				
				//String sql  = "update bidding set finalPrice = ? where intialPrice = ?";
				String sql = "UPDATE prod_bid AS pb JOIN product AS p ON (pb.id_product = p.id) JOIN bidding AS b ON (pb.id_bid = b.id) SET b.finalPrice = ?, pb.cust_Name = ?, pb.status = ? WHERE p.name = ? ";
				PreparedStatement prepare = con.prepareStatement(sql);
					
					prepare.setDouble(1, bidPrice);
					prepare.setString(2, name);
					prepare.setString(3, "In process");
					prepare.setString(4, Bidding.getName());
					int s = prepare.executeUpdate();
					
					System.out.println(s);
					System.out.println(Bidding.getName());
					execute = true;
					
				con.close();
				
				
			}else {
				
				JFrame frame = new JFrame();
				JOptionPane.showMessageDialog( frame ,
					    "Your Amount Need Exceed from Final Price",
					    "Inane error",
					    JOptionPane.ERROR_MESSAGE);
				execute = false;
			}
		}else {
			
			JFrame frame = new JFrame();
			JOptionPane.showMessageDialog( frame ,
				    "Your Amount Need Exceed from Intial Price",
				    "Inane error",
				    JOptionPane.ERROR_MESSAGE);
			execute = false;
		}
		
		
		return execute;
		
	}
	
public boolean deleteProduct(String name) throws Exception {
	
	boolean success = false;
	
	DatabaseConnection dbcon = new DatabaseConnection();
	Connection con = dbcon.getConnection();
	
	String sql = "SELECT * FROM prod_bid AS pd JOIN bidding AS b ON (pd.id_bid = b.id) JOIN product AS p ON (pd.id_product = p.id) where name = ?";
	PreparedStatement prepare = con.prepareStatement(sql);
	prepare.setString(1, name);
	ResultSet rs = prepare.executeQuery();
	
	if(rs.next()) {
		
		String mainID = rs.getString(1); // Main ID
		String bidID =  rs.getString(2); // Bid ID
		String prodID = rs.getString(3); // Product ID
		System.out.println(prodID);
		String sql_1 = "delete from prod_bid where id = ?";
		String sql_2 = "delete from bidding where id = ?";
		String sql_3 = "delete from product where id = ?";
		
		PreparedStatement preStatement = con.prepareStatement(sql_1);
		preStatement.setString(1, mainID);
		preStatement.executeUpdate();
		
		PreparedStatement preStatement1 = con.prepareStatement(sql_2);
		preStatement1.setString(1,bidID);
		preStatement1.executeUpdate();
		
		PreparedStatement preStatement11 = con.prepareStatement(sql_3);
		preStatement11.setString(1, prodID);
		preStatement11.executeUpdate();
		
		success = true;
				
	}

	System.out.println("Done");
	con.close();
	
	return success;
}
	
public ArrayList<bidding> getBidTable() throws Exception {
		
		ArrayList<bidding> products = new ArrayList<bidding>();
		
		DatabaseConnection dbcon = new DatabaseConnection();
		Connection con = dbcon.getConnection();
		
		//String sql = "select name,year,type,cond from product where name = ?";
		String sql = "SELECT * FROM prod_bid AS pd JOIN bidding AS b ON (pd.id_bid = b.id) JOIN product AS p ON (pd.id_product = p.id)";
		PreparedStatement prepare = con.prepareStatement(sql);
		ResultSet rs = prepare.executeQuery();
		
		while(rs.next()) {
			
			bidding Bidding = new bidding();
			Bidding.setName(rs.getString(13));
			//Bidding.setYear(rs.getInt(2));
			//Bidding.setType(rs.getString(3));
			//Bidding.setCondition(rs.getString(4));
			Bidding.setIntialPrice(rs.getString(8));
			Bidding.setFinalPrice(rs.getString(9));
			//Bidding.setStartTime(rs.getString(7));
			//Bidding.setEndTime(rs.getString(8));
			//Bidding.setName(rs.getString(10));
			//Bidding.setYear(rs.getInt(11));
			//Bidding.setType(rs.getString(12));
			//Bidding.setCondition(rs.getString(13));
			products.add(Bidding);
			//customers.add(Customer);
		}
		
		con.close();
		return products;
		
		
	}

public ArrayList<bidding> getBidDealer(String name) throws Exception {
	
	ArrayList<bidding> products = new ArrayList<bidding>();
	
	DatabaseConnection dbcon = new DatabaseConnection();
	Connection con = dbcon.getConnection();
	
	//String sql = "select name,year,type,cond from product where name = ?";
	String sql = "SELECT * FROM prod_bid AS pd JOIN bidding AS b ON (pd.id_bid = b.id) JOIN product AS p ON (pd.id_product = p.id) where dealer_name = ?";
	PreparedStatement prepare = con.prepareStatement(sql);
	prepare.setString(1, name);
	ResultSet rs = prepare.executeQuery();
	
	while(rs.next()) {
		
		bidding Bidding = new bidding();
		Bidding.setStatus(rs.getString(6));
		Bidding.setName(rs.getString(13));
//		Bidding.setYear(rs.getInt(2));
//		Bidding.setType(rs.getString(3));
//		Bidding.setCondition(rs.getString(4));
		Bidding.setIntialPrice(rs.getString(8));
		Bidding.setFinalPrice(rs.getString(9));
//		Bidding.setStartTime(rs.getString(7));
//		Bidding.setEndTime(rs.getString(8));
//		Bidding.setName(rs.getString(10));
//		Bidding.setYear(rs.getInt(11));
//		Bidding.setType(rs.getString(12));
//		Bidding.setCondition(rs.getString(13));
		products.add(Bidding);
		//customers.add(Customer);
	}
	
	con.close();
	return products;
	
	
}

public boolean updateProduct(bidding Bidding) throws Exception {
	
	boolean updated = false;
	
	DatabaseConnection dbcon = new DatabaseConnection();
	Connection con = dbcon.getConnection();
	
	// UPDATE prod_bid AS pb JOIN bidding AS b ON (pb.id_bid = b.id) JOIN product AS p ON (pb.id_product = p.id) SET b.finalPrice = ? WHERE p.name = ?
	
	//String sql  = "update bidding set finalPrice = ? where intialPrice = ?";
	String sql = "UPDATE prod_bid AS pb JOIN product AS p ON (pb.id_product = p.id) JOIN bidding AS b ON (pb.id_bid = b.id) SET p.year = ?, p.type = ?, p.cond = ?, b.intialPrice = ?, b.finalPrice = ?, b.startTime = ?, b.endTime = ? WHERE p.name = ? ";
	PreparedStatement prepare = con.prepareStatement(sql);
	prepare.setInt(1, Bidding.getYear());
	prepare.setString(2, Bidding.getType());
	prepare.setString(3, Bidding.getCondition());
	prepare.setString(4, Bidding.getIntialPrice());
	prepare.setString(5, Bidding.getFinalPrice());
	prepare.setString(6, Bidding.getStartTime());
	prepare.setString(7, Bidding.getEndTime());
	prepare.setString(8, Bidding.getName());
	prepare.executeUpdate();

		
	con.close();
	
	
	return updated;
	
}

public boolean updateStatus(bidding Bidding) throws Exception {
	
	boolean updated = false;
	
	DatabaseConnection dbcon = new DatabaseConnection();
	Connection con = dbcon.getConnection();
	
	// UPDATE prod_bid AS pb JOIN bidding AS b ON (pb.id_bid = b.id) JOIN product AS p ON (pb.id_product = p.id) SET b.finalPrice = ? WHERE p.name = ?
	
	//String sql  = "update bidding set finalPrice = ? where intialPrice = ?";
	String sql = "UPDATE prod_bid AS pb JOIN product AS p ON (pb.id_product = p.id) JOIN bidding AS b ON (pb.id_bid = b.id) SET pb.status = ? WHERE p.name = ? ";
	PreparedStatement prepare = con.prepareStatement(sql);
	prepare.setString(1, Bidding.getStatus());
	prepare.setString(2, Bidding.getName());
	prepare.executeUpdate();

		
	con.close();
	
	
	return updated;
	
}
	
//	public bidding viewBidProduct(String name) throws SQLException, Exception {
//		
//		bidding Bidding = new bidding();
//		
//		DatabaseConnection dbcon = new DatabaseConnection();
//		Connection con = dbcon.getConnection();
//		
//		String sql = "select name,year,type,cond from product where name = ?";
//		PreparedStatement prepare = con.prepareStatement(sql);
//		prepare.setString(1, name);
//		ResultSet rs = prepare.executeQuery();
//		
//		if(rs.next()) {
//			
//			
//			Bidding.setName(rs.getString(1));
//			Bidding.setYear(rs.getInt(2));
//			Bidding.setType(rs.getString(3));
//			Bidding.setCondition(rs.getString(4));
//			
//		}
//		
//		con.close();
//		return Bidding;
//	}
	
	/*
	 * public static void main(String[] args) {
	 * 
	 * AdminController adminController = new AdminController(); try {
	 * 
	 * admin Admin = new admin();
	 * 
	 * Admin.setfName("Admin001"); Admin.setPassword("admin1234");
	 * Admin.setEmail("admin001@utem.com");
	 * System.out.println(adminController.addProduct(Admin));
	 * System.out.println("Data Updated"); }catch (Exception e) {
	 * 
	 * e.printStackTrace(); } }
	 */
	/*
	public static void main(String[] args) {
		
		BiddingController controller = new BiddingController();
		//bidding Bidding = new bidding();
		
		try {
			for(bidding Bidding : controller.getBid("EngkuV1.0")) {
				  
				  System.out.println(Bidding.getCondition());
				  System.out.println(Bidding.getYear());
				  
		 }
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	*/
//	
//	public static void main(String[] args) {
//		
//		BiddingController controller = new BiddingController();
//		try {
//			
//			System.out.println(controller.deleteProduct("EngkuV3.0"));
//			
//		} catch (Exception e) {
//			// TODO: handle exception
//		}
//	}
	// TODO
	// Update
	// Delete

}
