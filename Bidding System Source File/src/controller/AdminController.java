package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import database.DatabaseConnection;
import model.admin;
import view.AdminPanel;
import view.DealerPanel;
import view.UserPanel;
import view.UserStartBid;

/**
 * @author Reborn
 *
 */
public class AdminController {

	public boolean findAdmin (admin Admin) throws Exception{
		
		boolean found = false;
		
		DatabaseConnection dbcon = new DatabaseConnection();
		Connection con = dbcon.getConnection();
		
		String sql = "select fName from admin where fName = ? ";
		PreparedStatement prepare = con.prepareStatement(sql);
		prepare.setString(1, Admin.getfName());
		ResultSet rs = prepare.executeQuery();
		
		if(rs.next()) {
			found = true;
		}
		
		con.close();
		return found;
	}
	
	public int addAdmin(admin Admin) throws Exception {

		int success = -1;
		
		DatabaseConnection dbcon = new DatabaseConnection();
		Connection con = dbcon.getConnection();
		
		String sql = "insert into admin (fName, password, email) values (?,?,?)";
		
		if(findAdmin(Admin) == false) {
			
			PreparedStatement prepare = con.prepareStatement(sql);
			
			prepare.setString(1, Admin.getfName());
			prepare.setString(2, Admin.getPassword());
			prepare.setString(3, Admin.getEmail());
			
			success = prepare.executeUpdate();
		}
		
		
		con.close();	
		return success;
	}
	
	
	// Soon will add as Interface Function
	
	public boolean loginMaster(String usr, String pwd) throws Exception {
		
		boolean found = false;
		
		DatabaseConnection dbcon = new DatabaseConnection();
		Connection con = dbcon.getConnection();
	
		String sql_admin = "select fName, password from admin where fName = ? and password = ?";
		String sql_cust = "select fName, password from customer where fName = ? and password = ?";
		String sql_dealer = "select fName, password from dealer where fName = ? and password = ?";
		
		PreparedStatement prepare_admin = con.prepareStatement(sql_admin);
		prepare_admin.setString(1, usr);
		prepare_admin.setString(2, pwd);
		ResultSet rs_admin = prepare_admin.executeQuery();
		
		PreparedStatement prepare_cust = con.prepareStatement(sql_cust);
		prepare_cust.setString(1, usr);
		prepare_cust.setString(2, pwd);
		ResultSet rs_cust = prepare_cust.executeQuery();
		
		PreparedStatement prepare_dealer = con.prepareStatement(sql_dealer);
		prepare_dealer.setString(1, usr);
		prepare_dealer.setString(2, pwd);
		ResultSet rs_dealer = prepare_dealer.executeQuery();
		
		if(rs_admin.next()) {
			
			found = true;
			AdminPanel adminPanel = new AdminPanel();
			adminPanel.Panel();
			
		}else if(rs_dealer.next()) {
			
			found = true;
			DealerPanel dealerPanel = new DealerPanel(usr);
			dealerPanel.dealerPanel(usr);
			
			
		}else if (rs_cust.next()) {
			
			found = true;
			UserPanel userPanel = new UserPanel(usr);
			userPanel.userPanel(usr);
		}
		
		return found;
	}
	
		public static boolean registerMaster(String name, String password, String email, String lvl) throws Exception {
			
			boolean success = false;
			
			DatabaseConnection dbcon = new DatabaseConnection();
			Connection con = dbcon.getConnection();
			
			String sql_dealer = "insert into dealer (fName, password, email) values (?,?,?)";
			String sql_customer = "insert into customer (fName, password, email) values (?,?,?)";
			
			if(lvl == "Dealer") {
				
				PreparedStatement prepare = con.prepareStatement(sql_dealer);
				
				prepare.setString(1, name);
				prepare.setString(2, password);
				prepare.setString(3, email);
				prepare.executeUpdate();
				
				success = true;
			}
			
			if(lvl == "Customer") {
				
				PreparedStatement prepare = con.prepareStatement(sql_customer);
				
				prepare.setString(1, name);
				prepare.setString(2, password);
				prepare.setString(3, email);
				prepare.executeUpdate();
				
				success = true;
			}
				
			con.close();
			
			return success;
		}
	
	}

	
	
	/*
	 * public static void main(String[] args) {
	 * 
	 * AdminController adminController = new AdminController(); try {
	 * 
	 * admin Admin = new admin();
	 * 
	 * Admin.setfName("Admin001"); Admin.setPassword("admin1234");
	 * Admin.setEmail("admin001@utem.com");
	 * System.out.println(adminController.addProduct(Admin));
	 * System.out.println("Data Updated"); }catch (Exception e) {
	 * 
	 * e.printStackTrace(); } }
	 */

