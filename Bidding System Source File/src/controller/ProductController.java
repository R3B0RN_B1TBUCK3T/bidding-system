package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import database.DatabaseConnection;
import model.product;

public class ProductController {

	public boolean findProduct (product Product) throws Exception{
		
		boolean found = false;
		
		DatabaseConnection dbcon = new DatabaseConnection();
		Connection con = dbcon.getConnection();
		
		String sql = "select name from product where name = ? ";
		PreparedStatement prepare = con.prepareStatement(sql);
		prepare.setString(1, Product.getName());
		ResultSet rs = prepare.executeQuery();
		
		if(rs.next()) {
			found = true;
		}
		
		con.close();
		return found;
	}
	
	public int addProduct(product Product) throws Exception {

		int success = -1;
		
		DatabaseConnection dbcon = new DatabaseConnection();
		Connection con = dbcon.getConnection();
		
		String sql = "insert into product (id,name,year,type,cond) values (?,?,?,?,?)";
		
		if(findProduct(Product) == false) {
			
			PreparedStatement prepare = con.prepareStatement(sql);
			prepare.setInt(1, Product.getId());
			prepare.setString(2, Product.getName());
			prepare.setInt(3, Product.getYear());
			prepare.setString(4, Product.getType());
			prepare.setString(5, Product.getCondition());
			success = prepare.executeUpdate();
		}
		
		
		con.close();	
		return success;
	}
	
	// List Down all the Product
	public ArrayList<product> getProducts() throws Exception {
		
		ArrayList<product> products = new ArrayList<product>();
		
		DatabaseConnection dbcon = new DatabaseConnection();
		Connection con = dbcon.getConnection();
		
		String sql = "select name,year,type,cond from product";
		PreparedStatement prepare = con.prepareStatement(sql);
		
		ResultSet rs = prepare.executeQuery();
		
		while(rs.next()) {
			
			product Product = new product();
			Product.setName(rs.getString(1));
			Product.setYear(rs.getInt(2));
			Product.setType(rs.getString(3));
			Product.setCondition(rs.getString(4));
			products.add(Product);
			//customers.add(Customer);
		}
		
		con.close();
		return products;
	}
	
	
	// Update Product
	public void updateProduct(product Product) throws Exception{
		
		DatabaseConnection dbcon = new DatabaseConnection();
		Connection con = dbcon.getConnection();
		
		String sql = "update product set year = ?, type = ?, cond = ? where name =  ?";
		
		//if(findCustomer(Customer) == true) {
			
			PreparedStatement prepare = con.prepareStatement(sql);
			
			prepare.setInt(1, Product.getYear());
			prepare.setString(2, Product.getType());
			prepare.setString(3, Product.getCondition());
			prepare.setString(4, Product.getName());
			prepare.executeUpdate();
		//}

		System.out.println("Done");
		con.close();
		
		
	}
	
	// Delete Product 
	public void  deleteProduct (product Product) throws Exception {
		
		DatabaseConnection dbcon = new DatabaseConnection();
		Connection con = dbcon.getConnection();
		
		String sql = "delete from product where name = ?";
		
		PreparedStatement prepare = con.prepareStatement(sql);
		prepare.setString(1, Product.getName());
		prepare.executeUpdate();
		
		System.out.println("Done");
		con.close();
	}
	
	/*
	 * public static void main(String[] args) {
	 * 
	 * ProductController productController = new ProductController(); try {
	 * 
	 * product Product = new product(); // Product.setId(5);
	 * Product.setName("Test0009"); Product.setYear(2019); Product.setType("Book");
	 * Product.setCondition("New");
	 * System.out.println(productController.addProduct(Product));
	 * System.out.println("Data Updated"); }catch (Exception e) {
	 * 
	 * e.printStackTrace(); } }
	 */
}
