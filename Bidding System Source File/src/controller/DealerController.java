package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import database.DatabaseConnection;
import model.dealer;

public class DealerController {

	public boolean findDealer(dealer Dealer) throws Exception{
		
		boolean found = false;
		
		DatabaseConnection dbcon = new DatabaseConnection();
		Connection con = dbcon.getConnection();
		
		String sql = "select fName from dealer where fName = ?";
		PreparedStatement prepare = con.prepareStatement(sql);
		prepare.setString(1, Dealer.getfName());
		ResultSet rs = prepare.executeQuery();
		
		if(rs.next()) {
			found = true;
		}
		
		con.close();
		return found;
	}
	
	public int addDealer(dealer Dealer) throws Exception{
		
		int success = -1;
		
		DatabaseConnection dbcon = new DatabaseConnection();
		Connection con = dbcon.getConnection();
		
		String sql = "insert into dealer (fName, password, email) values (?,?,?)";
		
		if(findDealer(Dealer)  == false) {
			
			PreparedStatement prepare = con.prepareStatement(sql);
			prepare.setString(1,Dealer.getfName());
			prepare.setString(2, Dealer.getPassword());
			prepare.setString(3, Dealer.getEmail());
			success = prepare.executeUpdate();
		}
		
		con.close();
		return success;
	}
	
	// List Down all the Dealer
	public ArrayList<dealer> getDealers() throws Exception {
		
		ArrayList<dealer> dealers = new ArrayList<dealer>();
		
		DatabaseConnection dbcon = new DatabaseConnection();
		Connection con = dbcon.getConnection();
		
		String sql = "select fname, email from dealer";
		PreparedStatement prepare = con.prepareStatement(sql);
		
		ResultSet rs = prepare.executeQuery();
		
		while(rs.next()) {
			
			dealer Dealer = new dealer();
			Dealer.setfName(rs.getString(1));
			Dealer.setEmail(rs.getString(2));
			dealers.add(Dealer);
		}
		
		con.close();
		return dealers;
	}
	
	
	// Update Dealer
	public void updateDealer (dealer Dealer) throws Exception{
		
		DatabaseConnection dbcon = new DatabaseConnection();
		Connection con = dbcon.getConnection();
		
		String sql = "update dealer set email = ?, password = ? where fName =  ?";
		
		//if(findCustomer(Customer) == true) {
			
			PreparedStatement prepare = con.prepareStatement(sql);
			prepare.setString(1, Dealer.getEmail());
			prepare.setString(2, Dealer.getPassword());
			prepare.setString(3, Dealer.getfName());
			prepare.executeUpdate();
		//}

		System.out.println("Done");
		con.close();
		
		
	}
	
	// Delete Dealer 
	public void  deleteDealer(dealer Dealer) throws Exception {
		
		DatabaseConnection dbcon = new DatabaseConnection();
		Connection con = dbcon.getConnection();
		
		String sql = "delete from dealer where fName = ?";
		
		PreparedStatement prepare = con.prepareStatement(sql);
		prepare.setString(1, Dealer.getfName());
		prepare.executeUpdate();
		
		System.out.println("Done");
		con.close();
	}
	
	/*
	 * public static void main(String[] args) {
	 * 
	 * DealerController dealerController = new DealerController(); try {
	 * 
	 * dealer Dealer = new dealer(); Dealer.setfName("Dealer001");
	 * Dealer.setPassword("dealer1234"); Dealer.setEmail("dealer001@utem.com");
	 * //System.out.println(dealerController.addDealer(Dealer));
	 * System.out.println(dealerController.findDealer(Dealer));
	 * System.out.println("Data Updated");
	 * 
	 * } catch (Exception e) { e.printStackTrace(); } }
	 */
	
	/*
	 * public static void main(String[] args) {
	 * 
	 * dealer Dealer = new dealer(); Dealer.setfName("Engku Aiman");
	 * Dealer.setEmail("engkuaiman0001@utem.com"); Dealer.setPassword("engku1234");
	 * DealerController dealerController = new DealerController(); try {
	 * 
	 * //dealerController.addDealer(Dealer);
	 * //dealerController.updateDealer(Dealer);
	 * //dealerController.deleteDealer(Dealer); for(dealer Dealer01:
	 * dealerController.getDealers()) {
	 * 
	 * System.out.println(Dealer01.getfName()); }
	 * 
	 * } catch (Exception e) { // TODO: handle exception } }
	 */
}
