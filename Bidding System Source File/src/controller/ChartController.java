package controller;

import java.awt.Color;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

import database.DatabaseConnection;

public class ChartController {


	public void generateChart() throws Exception {
		
		DatabaseConnection dbcon = new DatabaseConnection();
		Connection con = dbcon.getConnection();
		
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
//		Statement stmt = con.createStatement();
		
//		ResultSet rs = stmt.executeQuery("Select id, name FROM product");
		
		String sql_dealer = "SELECT COUNT(*) FROM dealer";
		String sql_customer = "SELECT COUNT(*) FROM customer";
		PreparedStatement prepare_dealer = con.prepareStatement(sql_dealer);
		PreparedStatement prepare_cust = con.prepareStatement(sql_customer);
		
		ResultSet rs_1	= prepare_dealer.executeQuery();
		ResultSet rs_2 = prepare_cust.executeQuery();
		
		
		if(rs_1.next() && rs_2.next()) {
			
			dataset.setValue(rs_1.getInt(1), "Dealer", "Dealer");
			dataset.setValue(rs_2.getInt(1) , "Customer", "Customer");
			
		}
		
		JFreeChart jFreeChart = ChartFactory.createBarChart("Total Dealer and Customer in UTeM Bidding System", "Category", "Number of Users", dataset, PlotOrientation.VERTICAL, true,true,false);
		
		CategoryPlot plot = jFreeChart.getCategoryPlot();
		plot.setRangeGridlinePaint(Color.black);
		
		ChartFrame chartFrame = new ChartFrame("Total Dealer and Customer in UTeM Bidding System", jFreeChart, true);
		chartFrame.setVisible(true);
		chartFrame.setSize(700, 700);
		
		
			
		}
	}