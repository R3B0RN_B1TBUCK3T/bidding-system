

package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.BiddingController;
import model.bidding;

import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextField;
import javax.swing.JButton;

/**
 * @author Reborn
 *
 */
public class UserViewBidDetalis extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JLabel lblInputname;

	/**
	 * Launch the application.
	 */
	public void BidDetalis(String msg, String name) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UserViewBidDetalis frame = new UserViewBidDetalis(msg , name);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public UserViewBidDetalis(String msg, String name) {
		BiddingController biddingController = new BiddingController();
		try {
			for (bidding bidDetalis: biddingController.getBid(msg) ) {
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setBounds(100, 100, 450, 470);
			contentPane = new JPanel();
			contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
			setContentPane(contentPane);
			contentPane.setLayout(null);
			
			JLabel lblName = new JLabel("Name");
			lblName.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblName.setBounds(10, 11, 126, 26);
			contentPane.add(lblName);
			
			JLabel label = new JLabel(":");
			label.setFont(new Font("Tahoma", Font.PLAIN, 14));
			label.setBounds(146, 11, 10, 26);
			contentPane.add(label);
			
			UserStartBid bid = new UserStartBid(name);
			
			lblInputname = new JLabel(bidDetalis.getName());
			lblInputname.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblInputname.setBounds(166, 13, 258, 24);
			contentPane.add(lblInputname);
			
			String newYear = Integer.toString(bidDetalis.getYear());
			JLabel lblInputyear = new JLabel(newYear);
			lblInputyear.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblInputyear.setBounds(166, 50, 258, 24);
			contentPane.add(lblInputyear);
			
			JLabel label_2 = new JLabel(":");
			label_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
			label_2.setBounds(146, 48, 10, 26);
			contentPane.add(label_2);
			
			JLabel lblYear = new JLabel("Year");
			lblYear.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblYear.setBounds(10, 48, 126, 26);
			contentPane.add(lblYear);
			
			JLabel lblInputtype = new JLabel(bidDetalis.getType());
			lblInputtype.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblInputtype.setBounds(166, 87, 258, 24);
			contentPane.add(lblInputtype);
			
			JLabel label_3 = new JLabel(":");
			label_3.setFont(new Font("Tahoma", Font.PLAIN, 14));
			label_3.setBounds(146, 85, 10, 26);
			contentPane.add(label_3);
			
			JLabel lblType = new JLabel("Type");
			lblType.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblType.setBounds(10, 85, 126, 26);
			contentPane.add(lblType);
			
			JLabel label_1 = new JLabel(bidDetalis.getCondition());
			label_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
			label_1.setBounds(166, 124, 258, 24);
			contentPane.add(label_1);
			
			JLabel label_4 = new JLabel(":");
			label_4.setFont(new Font("Tahoma", Font.PLAIN, 14));
			label_4.setBounds(146, 122, 10, 26);
			contentPane.add(label_4);
			
			JLabel lblCondition = new JLabel("Condition");
			lblCondition.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblCondition.setBounds(10, 122, 126, 26);
			contentPane.add(lblCondition);
			
			JLabel lblIntialPrice = new JLabel("Intial Price");
			lblIntialPrice.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblIntialPrice.setBounds(10, 159, 126, 26);
			contentPane.add(lblIntialPrice);
			
			JLabel label_6 = new JLabel(":");
			label_6.setFont(new Font("Tahoma", Font.PLAIN, 14));
			label_6.setBounds(146, 159, 10, 26);
			contentPane.add(label_6);
			
			JLabel lblInputintialprice = new JLabel(bidDetalis.getIntialPrice());
			lblInputintialprice.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblInputintialprice.setBounds(166, 161, 258, 24);
			contentPane.add(lblInputintialprice);
			
			JLabel lblFinalPrice = new JLabel("Final Price");
			lblFinalPrice.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblFinalPrice.setBounds(10, 196, 126, 26);
			contentPane.add(lblFinalPrice);
			
			JLabel label_7 = new JLabel(":");
			label_7.setFont(new Font("Tahoma", Font.PLAIN, 14));
			label_7.setBounds(146, 196, 10, 26);
			contentPane.add(label_7);
			
			JLabel lblInputfinalprice = new JLabel(bidDetalis.getFinalPrice());
			lblInputfinalprice.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblInputfinalprice.setBounds(166, 198, 258, 24);
			contentPane.add(lblInputfinalprice);
			
			JLabel lblStartTime = new JLabel("Start Date");
			lblStartTime.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblStartTime.setBounds(10, 233, 126, 26);
			contentPane.add(lblStartTime);
			
			JLabel label_8 = new JLabel(":");
			label_8.setFont(new Font("Tahoma", Font.PLAIN, 14));
			label_8.setBounds(146, 233, 10, 26);
			contentPane.add(label_8);
			
			JLabel lblInputstarttime = new JLabel(bidDetalis.getStartTime());
			lblInputstarttime.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblInputstarttime.setBounds(166, 235, 258, 24);
			contentPane.add(lblInputstarttime);
			
			JLabel lblEndTime = new JLabel("End Date");
			lblEndTime.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblEndTime.setBounds(10, 270, 126, 26);
			contentPane.add(lblEndTime);
			
			JLabel label_11 = new JLabel(":");
			label_11.setFont(new Font("Tahoma", Font.PLAIN, 14));
			label_11.setBounds(146, 270, 10, 26);
			contentPane.add(label_11);
			
			JLabel lblInputenddate = new JLabel(bidDetalis.getEndTime());
			lblInputenddate.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblInputenddate.setBounds(166, 272, 258, 24);
			contentPane.add(lblInputenddate);
			
			JLabel lblBidYourPrice = new JLabel("Bid Your Price");
			lblBidYourPrice.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 14));
			lblBidYourPrice.setBounds(10, 323, 126, 26);
			contentPane.add(lblBidYourPrice);
			
			JLabel lblRm = new JLabel("RM");
			lblRm.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblRm.setBounds(10, 350, 41, 26);
			contentPane.add(lblRm);
			
			textField = new JTextField();
			textField.setBounds(50, 350, 157, 26);
			contentPane.add(textField);
			textField.setColumns(10);
			
			JButton btnBidNow = new JButton("Bid Now");
			btnBidNow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					
					BiddingController controller = new BiddingController();
					bidding Bidding = new bidding();
					
					Bidding.setName(lblInputname.getText());
					Bidding.setIntialPrice(lblInputintialprice.getText());
					Bidding.setFinalPrice(lblInputfinalprice.getText());
					double price = Double.parseDouble(textField.getText());
					
					try {
						
						boolean x = controller.startBid(Bidding, price, name);
						
						if(x == true) {
							
							UserBidSuccess success = new UserBidSuccess(name);
							success.UserSuccess(name);
							//dispose();
						}else {
							
							UserViewBidDetalis bidDetalis = new UserViewBidDetalis(Bidding.getName(),name);
							bidDetalis.BidDetalis(Bidding.getName(),name);
							dispose();
						}
						
						
						
					} catch (Exception e2) {
						// TODO: handle exception
					}
					
					dispose();
				}
				});
				
				
			btnBidNow.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnBidNow.setBounds(10, 387, 89, 23);
			contentPane.add(btnBidNow);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
