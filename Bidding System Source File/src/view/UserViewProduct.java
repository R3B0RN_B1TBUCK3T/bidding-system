package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import controller.BiddingController;
import controller.CustomerController;
import model.bidding;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class UserViewProduct extends JFrame {

	private JPanel contentPane;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public void viewProduct(String name) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UserViewProduct frame = new UserViewProduct(name);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public UserViewProduct(String name) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 360);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblProductCenter = new JLabel("Product Center");
		lblProductCenter.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblProductCenter.setBounds(10, 11, 362, 23);
		contentPane.add(lblProductCenter);
		
//		table = new JTable();
//		table.setBounds(10, 51, 260, 199);
//		contentPane.add(table);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 70, 315, 240);
		contentPane.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		BiddingController controller = new BiddingController();
		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("Name");
		model.addColumn("Intial Price");
		model.addColumn("Final Price");
		
		try {
			for(bidding Bidding : controller.getBidTable()) {
				model.addRow(new Object[] {
						
						Bidding.getName(),
						Bidding.getIntialPrice(),
						Bidding.getFinalPrice()
						
				});
			}
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		table.setModel(model);
		
		JButton btnStartBid = new JButton("Start Bid");
		btnStartBid.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				UserStartBid.startBid(name);
				dispose();
			}
			
		});
		btnStartBid.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnStartBid.setBounds(335, 70, 89, 34);
		contentPane.add(btnStartBid);
		
		JButton button = new JButton("<< Back");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				UserPanel panel = new UserPanel(name);
				panel.userPanel(name);
				dispose();
			}
		});
		button.setBounds(335, 115, 89, 34);
		contentPane.add(button);
	}
}
