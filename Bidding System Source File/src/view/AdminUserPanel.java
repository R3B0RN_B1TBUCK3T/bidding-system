package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import controller.CustomerController;
import controller.DealerController;
import model.customer;
import model.dealer;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;

public class AdminUserPanel extends JFrame {

	private JPanel contentPane;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public void UserPanel() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdminUserPanel frame = new AdminUserPanel();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AdminUserPanel() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 360);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblUsers = new JLabel("Users Panel");
		lblUsers.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblUsers.setBounds(10, 11, 155, 23);
		contentPane.add(lblUsers);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 70, 315, 240);
		contentPane.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		CustomerController controller = new CustomerController();
		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("Name");
		model.addColumn("Email");
		
		try {
			for(customer Customers: controller.getCustomers()) {
				model.addRow(new Object[] {
					Customers.getfName(),
					Customers.getEmail()
					
				});
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		table.setModel(model);
//		table.getColumnModel().getColumn(0).setPreferredWidth(100);
//		table.getColumnModel().getColumn(0).setMinWidth(100);
//		table.getColumnModel().getColumn(0).setMaxWidth(100);
//		table.getColumnModel().getColumn(1).setPreferredWidth(100);
//		table.getColumnModel().getColumn(1).setMinWidth(100);
//		table.getColumnModel().getColumn(1).setMaxWidth(100);
//		
		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				AdminUserAdd userAdd = new AdminUserAdd();
				userAdd.UserAdd();
				dispose();
			}
		});
		btnAdd.setBounds(335, 70, 89, 34);
		contentPane.add(btnAdd);
		
		JButton btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				AdminUserUpdate userUpdate = new AdminUserUpdate();
				userUpdate.UserUpdate();
				dispose();
			}
		});
		btnUpdate.setBounds(335, 115, 89, 34);
		contentPane.add(btnUpdate);
		
		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				AdminUserDelete userDelete = new AdminUserDelete();
				userDelete.UserDelete();
				dispose();
			}
		});
		btnDelete.setBounds(335, 160, 89, 34);
		contentPane.add(btnDelete);
		
		JButton button = new JButton("<< Back");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				AdminPanel adminPanel = new AdminPanel();
				adminPanel.Panel();
				dispose();
			}
		});
		button.setBounds(335, 205, 89, 34);
		contentPane.add(button);
		
		JLabel label = new JLabel("@ Admin");
		label.setBounds(10, 38, 73, 21);
		contentPane.add(label);
	}
}
