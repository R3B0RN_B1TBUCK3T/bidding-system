package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.AdminController;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MasterRegister extends JFrame {

	private JPanel contentPane;
	private JTextField txtName;
	private JTextField txtPwd;
	private JTextField txtEmail;

	/**
	 * Launch the application.
	 */
	public static void Regsiter() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MasterRegister frame = new MasterRegister();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MasterRegister() {
		setTitle("Registration Panel");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 318);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Registration");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblNewLabel.setBounds(10, 11, 221, 39);
		contentPane.add(lblNewLabel);
		
		JLabel lblName = new JLabel("Name");
		lblName.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblName.setBounds(10, 71, 80, 28);
		contentPane.add(lblName);
		
		JLabel label = new JLabel(":");
		label.setFont(new Font("Tahoma", Font.PLAIN, 16));
		label.setBounds(93, 71, 6, 28);
		contentPane.add(label);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblPassword.setBounds(10, 110, 80, 28);
		contentPane.add(lblPassword);
		
		JLabel label_2 = new JLabel(":");
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 16));
		label_2.setBounds(93, 110, 6, 28);
		contentPane.add(label_2);
		
		JLabel lblEmail = new JLabel("Email");
		lblEmail.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblEmail.setBounds(10, 149, 80, 28);
		contentPane.add(lblEmail);
		
		JLabel label_3 = new JLabel(":");
		label_3.setFont(new Font("Tahoma", Font.PLAIN, 16));
		label_3.setBounds(93, 149, 6, 28);
		contentPane.add(label_3);
		
		JLabel lblCategory = new JLabel("Category");
		lblCategory.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblCategory.setBounds(10, 188, 80, 28);
		contentPane.add(lblCategory);
		
		JLabel label_4 = new JLabel(":");
		label_4.setFont(new Font("Tahoma", Font.PLAIN, 16));
		label_4.setBounds(93, 188, 6, 28);
		contentPane.add(label_4);
		
		txtName = new JTextField();
		txtName.setBounds(109, 73, 266, 28);
		contentPane.add(txtName);
		txtName.setColumns(10);
		
		txtPwd = new JTextField();
		txtPwd.setColumns(10);
		txtPwd.setBounds(109, 110, 266, 28);
		contentPane.add(txtPwd);
		
		txtEmail = new JTextField();
		txtEmail.setColumns(10);
		txtEmail.setBounds(109, 149, 266, 28);
		contentPane.add(txtEmail);
		
		JComboBox cBox_Cat = new JComboBox();
		cBox_Cat.addItem("Dealer");
		cBox_Cat.addItem("Customer");
		cBox_Cat.setSelectedItem(null);
		cBox_Cat.setBounds(109, 188, 266, 28);
		contentPane.add(cBox_Cat);
		
		JButton btnNewButton = new JButton("Register Now");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String name = txtName.getText();
				String pwd = txtPwd.getText();
				String email = txtEmail.getText();
				String category = (String)cBox_Cat.getSelectedItem();
			
				try {
					if(AdminController.registerMaster(name, pwd, email, category) == true) {
						
						MasterLogin.Login();
						dispose();
					}
					
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnNewButton.setBounds(109, 245, 122, 23);
		contentPane.add(btnNewButton);
		
		JButton btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				MasterLogin.Login();
				dispose();
			}
		});
		btnExit.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnExit.setBounds(251, 245, 122, 23);
		contentPane.add(btnExit);
	}
}
