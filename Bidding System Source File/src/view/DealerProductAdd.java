package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.BiddingController;
import controller.CustomerController;
import controller.DealerController;
import controller.ProductController;
import model.bidding;
import model.customer;
import model.dealer;
import model.product;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.awt.event.ActionEvent;
import com.toedter.calendar.JDateChooser;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class DealerProductAdd extends JFrame {

	private JPanel contentPane;
	private JTextField txtName;
	private JTextField txtYear;
	private JTextField txtType;
	private JTextField txtInPrice;
	private JTextField txtEndPrice;
	private JComboBox comB_Cond;
	private JDateChooser DC_endTime;
	private JDateChooser DC_startTime;

	/**
	 * Launch the application.
	 */
	public void ProductAdd (String name) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DealerProductAdd frame = new DealerProductAdd(name);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DealerProductAdd(String name) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 425);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblAddNewUser = new JLabel("Add Product");
		lblAddNewUser.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblAddNewUser.setBounds(10, 11, 241, 29);
		contentPane.add(lblAddNewUser);
		
		JLabel lblName = new JLabel("Name");
		lblName.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblName.setBounds(10, 72, 46, 14);
		contentPane.add(lblName);
		
		JLabel lblEmail = new JLabel("Year");
		lblEmail.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblEmail.setBounds(10, 105, 46, 14);
		contentPane.add(lblEmail);
		
		JLabel lblPassword = new JLabel("Type");
		lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblPassword.setBounds(10, 138, 71, 14);
		contentPane.add(lblPassword);
		
		JLabel label = new JLabel(":");
		label.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label.setBounds(94, 72, 16, 14);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel(":");
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_1.setBounds(94, 105, 16, 14);
		contentPane.add(label_1);
		
		JLabel label_2 = new JLabel(":");
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_2.setBounds(94, 138, 16, 14);
		contentPane.add(label_2);
		
		txtName = new JTextField();
		txtName.setBounds(104, 71, 207, 20);
		contentPane.add(txtName);
		txtName.setColumns(10);
		
		txtYear = new JTextField();
		txtYear.setColumns(10);
		txtYear.setBounds(104, 104, 207, 20);
		contentPane.add(txtYear);
		
		txtType = new JTextField();
		txtType.setColumns(10);
		txtType.setBounds(104, 137, 207, 20);
		contentPane.add(txtType);
		
		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String nameprod = txtName.getText();
				String year = txtYear.getText();
				int newYear = Integer.parseInt(year);
				
				String type = txtType.getText(); 
				
				String condition = (String)comB_Cond.getSelectedItem();
				
				String intialPrice = txtInPrice.getText();
				String finalPrice = txtEndPrice.getText();
				
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				String startTime = df.format(DC_startTime.getDate());
				String endTime = df.format(DC_endTime.getDate());
				
				bidding Bidding = new bidding();
				Bidding.setCondition(condition);
				Bidding.setEndTime(endTime);
				Bidding.setFinalPrice(finalPrice);
				Bidding.setIntialPrice(intialPrice);
				Bidding.setName(nameprod);
				Bidding.setStartTime(startTime);
				Bidding.setType(type);
				Bidding.setYear(newYear);
				
				
				
				BiddingController biddingController = new BiddingController();
				DealerProductSuccess success = new DealerProductSuccess(name);
				
				try {
					biddingController.addBid(Bidding,name,"");
					success.DealerSuccess(name);
					dispose();
					//txtName.setText(condValue);
//					txtName.setText("");
//					txtYear.setText("");
//					txtType.setText("");
//					comB_Cond.setSelectedItem(null);
//					txtInPrice.setText("");
//					txtEndPrice.setText("");
//					DC_startTime.setDate(null);
//					DC_endTime.setDate(null);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});
		btnAdd.setBounds(104, 335, 89, 34);
		contentPane.add(btnAdd);
		
		JButton btnClear = new JButton("Back");
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
//				txtName.setText("");
//				txtYear.setText("");
//				txtType.setText("");
//				comB_Cond.setSelectedItem(null);
//				txtInPrice.setText("");
//				txtEndPrice.setText("");
//				DC_startTime.setDate(null);
//				DC_endTime.setDate(null);
//				
				
				DealerProductPanel dealerProductPanel = new DealerProductPanel(name);
				dealerProductPanel.ProductPanel(name);
				dispose();
			}
		});
		btnClear.setBounds(222, 335, 89, 34);
		contentPane.add(btnClear);
		
		JLabel lblCondition = new JLabel("Condition");
		lblCondition.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblCondition.setBounds(10, 171, 71, 14);
		contentPane.add(lblCondition);
		
		JLabel label_4 = new JLabel(":");
		label_4.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_4.setBounds(94, 171, 16, 14);
		contentPane.add(label_4);
		
		JLabel lblIntialPrice = new JLabel("Intial Price");
		lblIntialPrice.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblIntialPrice.setBounds(10, 204, 71, 14);
		contentPane.add(lblIntialPrice);
		
		JLabel label_5 = new JLabel(":");
		label_5.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_5.setBounds(94, 204, 16, 14);
		contentPane.add(label_5);
		
		txtInPrice = new JTextField();
		txtInPrice.setColumns(10);
		txtInPrice.setBounds(131, 203, 180, 20);
		contentPane.add(txtInPrice);
		
		JLabel lblEndPrice = new JLabel("Final Price");
		lblEndPrice.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblEndPrice.setBounds(10, 237, 71, 14);
		contentPane.add(lblEndPrice);
		
		JLabel label_6 = new JLabel(":");
		label_6.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_6.setBounds(94, 237, 16, 14);
		contentPane.add(label_6);
		
		txtEndPrice = new JTextField();
		txtEndPrice.setColumns(10);
		txtEndPrice.setBounds(131, 236, 180, 20);
		contentPane.add(txtEndPrice);
		
		JLabel lblStart = new JLabel("Start Time");
		lblStart.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblStart.setBounds(10, 269, 71, 14);
		contentPane.add(lblStart);
		
		JLabel label_7 = new JLabel(":");
		label_7.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_7.setBounds(94, 269, 16, 14);
		contentPane.add(label_7);
		
		JLabel lblRm = new JLabel("RM");
		lblRm.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblRm.setBounds(104, 204, 24, 14);
		contentPane.add(lblRm);
		
		JLabel label_3 = new JLabel("RM");
		label_3.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_3.setBounds(104, 237, 24, 14);
		contentPane.add(label_3);
		
		JLabel lblEndTime = new JLabel("End Time");
		lblEndTime.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblEndTime.setBounds(10, 302, 71, 14);
		contentPane.add(lblEndTime);
		
		DC_endTime = new JDateChooser();
		DC_endTime.setBounds(104, 302, 207, 20);
		contentPane.add(DC_endTime);
		
		JLabel label_9 = new JLabel(":");
		label_9.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_9.setBounds(94, 302, 16, 14);
		contentPane.add(label_9);
		
		comB_Cond = new JComboBox();

		comB_Cond.addItem("New");
		comB_Cond.addItem("Used");
		comB_Cond.setSelectedItem(null);
		comB_Cond.setBounds(104, 170, 207, 20);
		contentPane.add(comB_Cond);
		
		DC_startTime = new JDateChooser();
		DC_startTime.setBounds(104, 269, 207, 20);
		contentPane.add(DC_startTime);
	}

}
