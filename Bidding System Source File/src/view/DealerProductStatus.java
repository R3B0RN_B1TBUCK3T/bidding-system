package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.table.DefaultTableModel;

import controller.BiddingController;
import model.bidding;

import javax.swing.JScrollPane;

public class DealerProductStatus extends JFrame {

	private JPanel contentPane;
	private JTable table;

	/**
	 * Launch the application.
	 */
	
	// ProductPanel
	public void ProductStatus(String name) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DealerProductStatus frame = new DealerProductStatus(name);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DealerProductStatus(String name) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 360);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblUsers = new JLabel("Status");
		lblUsers.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblUsers.setBounds(10, 11, 155, 23);
		contentPane.add(lblUsers);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 70, 305, 240);
		contentPane.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		BiddingController controller = new BiddingController();
		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("Name");
		model.addColumn("Final Price");
		model.addColumn("Status");
		
		try {
			for(bidding Bidding : controller.getBidDealer(name)) {
				model.addRow(new Object[] {
						
						Bidding.getName(),
						Bidding.getFinalPrice(),
						Bidding.getStatus()
						
				});
			}
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		table.setModel(model);
//		table.getColumnModel().getColumn(0).setPreferredWidth(150);
//		table.getColumnModel().getColumn(0).setMinWidth(100);
//		table.getColumnModel().getColumn(0).setMaxWidth(151);
//		table.getColumnModel().getColumn(1).setPreferredWidth(100);
//		table.getColumnModel().getColumn(1).setMinWidth(50);
//		table.getColumnModel().getColumn(1).setMaxWidth(101);
//		table.getColumnModel().getColumn(2).setPreferredWidth(100);
//		table.getColumnModel().getColumn(2).setMinWidth(50);
//		table.getColumnModel().getColumn(2).setMaxWidth(101);
		
		JButton btnAdd = new JButton("Response");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				DealerProductStatusAction statusAction = new DealerProductStatusAction(name);
				statusAction.StatusAction(name);
				dispose();
			}
		});
		btnAdd.setBounds(325, 70, 99, 34);
		contentPane.add(btnAdd);
		
		JButton button = new JButton("<< Back");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				DealerPanel dealerPanel = new DealerPanel(name);
				dealerPanel.dealerPanel(name);
				dispose();
			}
		});
		button.setBounds(325, 115, 99, 34);
		contentPane.add(button);
	}
}
