package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AdminDealerSuccess extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public void DealerSuccess ( ) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdminDealerSuccess frame = new AdminDealerSuccess();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AdminDealerSuccess() {
		setTitle("Successfully Recorded");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 182);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblSuccessfully = new JLabel("Successfully Recorded");
		lblSuccessfully.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblSuccessfully.setBounds(124, 23, 195, 35);
		contentPane.add(lblSuccessfully);
		
		JButton btnOk = new JButton("Ok");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				AdminDealerPanel adminDealerPanel = new AdminDealerPanel();
				adminDealerPanel.DealerPanel();
				dispose();
			}
		});
		btnOk.setBounds(164, 76, 106, 34);
		contentPane.add(btnOk);
	}
}
