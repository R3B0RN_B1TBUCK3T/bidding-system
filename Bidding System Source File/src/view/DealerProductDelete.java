package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.BiddingController;
import controller.DealerController;
import model.dealer;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class DealerProductDelete extends JFrame {

	private JPanel contentPane;
	private JTextField txtName;

	/**
	 * Launch the application.
	 */
	public void ProductDelete(String name) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DealerProductDelete frame = new DealerProductDelete(name);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DealerProductDelete(String name) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 215);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblDeleteDealer = new JLabel("Delete Product");
		lblDeleteDealer.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblDeleteDealer.setBounds(10, 11, 202, 29);
		contentPane.add(lblDeleteDealer);
		
		JLabel lblName = new JLabel("Name");
		lblName.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblName.setBounds(10, 69, 46, 14);
		contentPane.add(lblName);
		
		JLabel label = new JLabel(":");
		label.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label.setBounds(94, 69, 12, 14);
		contentPane.add(label);
		
		txtName = new JTextField();
		txtName.setBounds(104, 68, 297, 20);
		contentPane.add(txtName);
		txtName.setColumns(10);
		
		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				dealer Dealer = new dealer();
				String fname = txtName.getText();
				Dealer.setfName(fname);
				BiddingController controller = new BiddingController();
				//AdminDealerSuccess dealerSuccess = new AdminDealerSuccess();
				DealerProductSuccess success = new DealerProductSuccess(name);
				
				try {
					controller.deleteProduct(fname);
					//DealerProductPanel dealerProductPanel = new DealerProductPanel(name);
					//dealerProductPanel.ProductPanel(name);
					success.DealerSuccess(name);
					dispose();
					//txtName.setText("");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnDelete.setBounds(104, 118, 89, 34);
		contentPane.add(btnDelete);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				DealerProductPanel dealerProductPanel = new DealerProductPanel(name);
				dealerProductPanel.ProductPanel(name);
				dispose();
			}
		});
		btnBack.setBounds(222, 118, 89, 34);
		contentPane.add(btnBack);
	}
}
