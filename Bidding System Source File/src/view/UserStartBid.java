package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.BiddingController;
import model.bidding;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * @author Reborn
 *
 */
public class UserStartBid extends JFrame {

	private JPanel contentPane;
	private JTextField txtProduct;
	private JButton btnBack;

	/**
	 * Launch the application.
	 */
	public static void startBid(String name) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UserStartBid frame = new UserStartBid(name);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public UserStartBid(String name) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 215);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblSelectProduct = new JLabel("Select Product");
		lblSelectProduct.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblSelectProduct.setBounds(137, 11, 159, 29);
		contentPane.add(lblSelectProduct);
		
		txtProduct = new JTextField();
		txtProduct.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtProduct.setBounds(10, 63, 414, 29);
		contentPane.add(txtProduct);
		txtProduct.setColumns(10);
		
		JButton btnOk = new JButton("Ok");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			
				String msg = txtProduct.getText();
				
				UserViewBidDetalis bidDetalis = new UserViewBidDetalis(msg,name);
				bidDetalis.BidDetalis(msg,name);
				dispose();
			}
		});
		btnOk.setBounds(109, 131, 89, 34);
		contentPane.add(btnOk);
		
		btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				UserViewProduct product = new UserViewProduct(name);
				product.viewProduct(name);
				dispose();
			}
		});
		btnBack.setBounds(238, 131, 89, 34);
		contentPane.add(btnBack);
	}
}
