package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class DealerPanel extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public void dealerPanel(String name) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DealerPanel frame = new DealerPanel(name);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DealerPanel(String name) {
		setTitle("Dealer Panel");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 360);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblWelcomeUser = new JLabel("Welcome " + name);
		lblWelcomeUser.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblWelcomeUser.setBounds(10, 21, 318, 29);
		contentPane.add(lblWelcomeUser);
		
		JButton btnViewProduct = new JButton("Product Center");
		btnViewProduct.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				DealerProductPanel dealerProductPanel = new DealerProductPanel(name);
				dealerProductPanel.ProductPanel(name);
				dispose();
			}
		});
		btnViewProduct.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnViewProduct.setBounds(92, 119, 252, 34);
		contentPane.add(btnViewProduct);
		
		JButton btnViewStatus = new JButton("Status");
		btnViewStatus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				DealerProductStatus status = new DealerProductStatus(name);
				status.ProductStatus(name);
				dispose();
			}
		});
		btnViewStatus.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnViewStatus.setBounds(92, 164, 252, 34);
		contentPane.add(btnViewStatus);
		
		JButton btnLogout = new JButton("Logout");
		btnLogout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				MasterLogin.Login();
				dispose();
			}
		});
		btnLogout.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnLogout.setBounds(92, 209, 252, 34);
		contentPane.add(btnLogout);
		
		JLabel lblDealer = new JLabel("@ Dealer");
		lblDealer.setBounds(10, 54, 73, 21);
		contentPane.add(lblDealer);
	}
}
