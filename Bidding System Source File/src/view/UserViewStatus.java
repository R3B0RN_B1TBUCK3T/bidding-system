package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import controller.BiddingController;
import controller.CustomerController;
import model.bidding;

import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class UserViewStatus extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JLabel lblYourStatus;
	private JButton btnNewButton;

	/**
	 * Launch the application.
	 */
	public void ViewStatus(String name) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UserViewStatus frame = new UserViewStatus(name);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public UserViewStatus(String name) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 353);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 51, 414, 199);
		contentPane.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		CustomerController controller = new CustomerController();
		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("Name");
		model.addColumn("Final Price");
		model.addColumn("Status");
		
		try {
			for(bidding Bidding : controller.getBidCustomer(name)) {
				model.addRow(new Object[] {
						
						Bidding.getName(),
						Bidding.getFinalPrice(),
						Bidding.getStatus()
				});
			}
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		table.setModel(model/*
						 * new DefaultTableModel( new Object[][] {
						 * 
						 * }, new String[] { "Name", "Intial Price", "Final Price" } ) { Class[]
						 * columnTypes = new Class[] { String.class, String.class, String.class };
						 * public Class getColumnClass(int columnIndex) { return
						 * columnTypes[columnIndex]; } }
						 */);
		
		lblYourStatus = new JLabel("Your Status");
		lblYourStatus.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblYourStatus.setBounds(10, 11, 305, 29);
		contentPane.add(lblYourStatus);
		
		btnNewButton = new JButton("Back");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				UserPanel panel = new UserPanel(name);
				panel.userPanel(name);
				dispose();
			}
		});
		btnNewButton.setBounds(335, 269, 89, 34);
		contentPane.add(btnNewButton);
		
		
//		table.getColumnModel().getColumn(0).setPreferredWidth(150);
//		table.getColumnModel().getColumn(0).setMinWidth(100);
//		table.getColumnModel().getColumn(0).setMaxWidth(151);
//		table.getColumnModel().getColumn(1).setPreferredWidth(100);
//		table.getColumnModel().getColumn(1).setMinWidth(50);
//		table.getColumnModel().getColumn(1).setMaxWidth(101);
//		table.getColumnModel().getColumn(2).setPreferredWidth(100);
//		table.getColumnModel().getColumn(2).setMinWidth(50);
//		table.getColumnModel().getColumn(2).setMaxWidth(101);
	}
}
