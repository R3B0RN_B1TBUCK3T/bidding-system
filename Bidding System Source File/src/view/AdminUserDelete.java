package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.CustomerController;
import controller.DealerController;
import model.customer;
import model.dealer;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AdminUserDelete extends JFrame {

	private JPanel contentPane;
	private JTextField txtName;

	/**
	 * Launch the application.
	 */
	public void UserDelete() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdminUserDelete frame = new AdminUserDelete();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AdminUserDelete() {
		setTitle("Delete User");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 215);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblDeleteDealer = new JLabel("Delete User");
		lblDeleteDealer.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblDeleteDealer.setBounds(10, 11, 151, 29);
		contentPane.add(lblDeleteDealer);
		
		JLabel lblName = new JLabel("Name");
		lblName.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblName.setBounds(10, 74, 46, 14);
		contentPane.add(lblName);
		
		JLabel label = new JLabel(":");
		label.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label.setBounds(93, 74, 12, 14);
		contentPane.add(label);
		
		txtName = new JTextField();
		txtName.setBounds(108, 73, 297, 20);
		contentPane.add(txtName);
		txtName.setColumns(10);
		
		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				customer Customer = new customer();
				String name = txtName.getText();
				Customer.setfName(name);
				CustomerController customerController = new CustomerController();
				AdminUserSuccess userSuccess = new AdminUserSuccess();
				
				try {
					customerController.deleteCustomer(Customer);
					userSuccess.UserSuccess();
					dispose();
					//txtName.setText("");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnDelete.setBounds(108, 127, 89, 34);
		contentPane.add(btnDelete);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				AdminUserPanel adminUserPanel = new AdminUserPanel();
				adminUserPanel.UserPanel();
				dispose();
			}
		});
		btnBack.setBounds(216, 127, 89, 34);
		contentPane.add(btnBack);
		
		JLabel label_1 = new JLabel("@ Admin");
		label_1.setBounds(10, 39, 73, 21);
		contentPane.add(label_1);
	}
}
