package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.ChartController;

import javax.swing.JLabel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Font;
import java.awt.Insets;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AdminPanel extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					AdminPanel frame = new AdminPanel();
//					frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}
//	
	public void Panel() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdminPanel frame = new AdminPanel();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AdminPanel() {
		setTitle("Admin Panel");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 360);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblWelcomeAdmin = new JLabel("Welcome Admin");
		lblWelcomeAdmin.setBounds(10, 11, 318, 29);
		lblWelcomeAdmin.setFont(new Font("Tahoma", Font.PLAIN, 24));
		contentPane.add(lblWelcomeAdmin);
		
		JButton btnManageUser = new JButton("Manage User");
		btnManageUser.setBounds(92, 74, 252, 34);
		btnManageUser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				AdminUserPanel adminUserPanel = new AdminUserPanel();
				adminUserPanel.UserPanel();
				dispose();
			}
		});
		contentPane.add(btnManageUser);
		
		JButton btnManageDealer = new JButton("Manage Dealer");
		btnManageDealer.setBounds(92, 119, 252, 34);
		btnManageDealer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				AdminDealerPanel adminDealerPanel = new AdminDealerPanel();
				adminDealerPanel.DealerPanel();
				dispose();
			}
		});
		contentPane.add(btnManageDealer);
		
		JButton btnManageProduct = new JButton("Manage Product");
		btnManageProduct.setBounds(92, 164, 252, 34);
		btnManageProduct.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				AdminProductPanel adminProductPanel = new AdminProductPanel();
				adminProductPanel.ProductPanel();
				dispose();
				
			}
		});
		contentPane.add(btnManageProduct);
		
		JButton btnLogout = new JButton("Logout");
		btnLogout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				MasterLogin.Login();
				dispose();
			}
		});
		btnLogout.setBounds(92, 254, 252, 34);
		contentPane.add(btnLogout);
		
		JButton btnGenerateStat = new JButton("Generate Statistics");
		btnGenerateStat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				ChartController controller = new ChartController();
				try {
					controller.generateChart();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		btnGenerateStat.setBounds(92, 209, 252, 34);
		contentPane.add(btnGenerateStat);
		
		JLabel lblAdmin = new JLabel("@ Admin");
		lblAdmin.setBounds(10, 43, 73, 21);
		contentPane.add(lblAdmin);
	}
}
