package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.CustomerController;
import controller.DealerController;
import model.customer;
import model.dealer;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AdminDealerAdd extends JFrame {

	private JPanel contentPane;
	private JTextField txtName;
	private JTextField txtEmail;
	private JTextField txtPassword;

	/**
	 * Launch the application.
	 */
	public void DealerAdd () {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdminDealerAdd frame = new AdminDealerAdd();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AdminDealerAdd() {
		setTitle("Add Dealer");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 285);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblAddNewUser = new JLabel("Add Dealer");
		lblAddNewUser.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblAddNewUser.setBounds(10, 11, 241, 29);
		contentPane.add(lblAddNewUser);
		
		JLabel lblName = new JLabel("Name");
		lblName.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblName.setBounds(10, 72, 46, 14);
		contentPane.add(lblName);
		
		JLabel lblEmail = new JLabel("Email");
		lblEmail.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblEmail.setBounds(10, 103, 46, 14);
		contentPane.add(lblEmail);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblPassword.setBounds(10, 140, 71, 14);
		contentPane.add(lblPassword);
		
		JLabel label = new JLabel(":");
		label.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label.setBounds(94, 72, 16, 14);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel(":");
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_1.setBounds(94, 105, 16, 14);
		contentPane.add(label_1);
		
		JLabel label_2 = new JLabel(":");
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_2.setBounds(91, 140, 16, 14);
		contentPane.add(label_2);
		
		txtName = new JTextField();
		txtName.setBounds(104, 71, 207, 20);
		contentPane.add(txtName);
		txtName.setColumns(10);
		
		txtEmail = new JTextField();
		txtEmail.setColumns(10);
		txtEmail.setBounds(104, 104, 207, 20);
		contentPane.add(txtEmail);
		
		txtPassword = new JTextField();
		txtPassword.setColumns(10);
		txtPassword.setBounds(104, 139, 207, 20);
		contentPane.add(txtPassword);
		
		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String name = txtName.getText();
				String email = txtEmail.getText();
				String password = txtPassword.getText();
				
				dealer Dealer = new dealer();
				Dealer.setfName(name);
				Dealer.setEmail(email);
				Dealer.setPassword(password);
				
				DealerController dealerController = new DealerController();
				AdminDealerSuccess dealerSuccess = new AdminDealerSuccess();
				
				try {
					dealerController.addDealer(Dealer);
					dealerSuccess.DealerSuccess();
					dispose();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});
		btnAdd.setBounds(104, 187, 89, 34);
		contentPane.add(btnAdd);
		
		JButton btnClear = new JButton("Back");
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
//				txtName.setText("");
//				txtEmail.setText("");
//				txtPassword.setText("");
				AdminDealerPanel dealerPanel = new AdminDealerPanel();
				dealerPanel.DealerPanel();
				dispose();
			}
		});
		btnClear.setBounds(222, 187, 89, 34);
		contentPane.add(btnClear);
		
		JLabel label_3 = new JLabel("@ Admin");
		label_3.setBounds(8, 40, 73, 21);
		contentPane.add(label_3);
	}
}
