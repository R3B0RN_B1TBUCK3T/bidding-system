package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.table.DefaultTableModel;

import controller.BiddingController;
import model.bidding;

import javax.swing.JScrollPane;

public class AdminProductPanel extends JFrame {

	private JPanel contentPane;
	private JTable table;

	/**
	 * Launch the application.
	 */
	
	// ProductPanel
	public void ProductPanel() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdminProductPanel frame = new AdminProductPanel();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AdminProductPanel() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 360);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblUsers = new JLabel("Product Panel");
		lblUsers.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblUsers.setBounds(10, 11, 155, 23);
		contentPane.add(lblUsers);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 70, 315, 240);
		contentPane.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		BiddingController controller = new BiddingController();
		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("Name");
		model.addColumn("Intial Price");
		model.addColumn("Final Price");
		
		try {
			for(bidding Bidding : controller.getBidTable()) {
				model.addRow(new Object[] {
						
						Bidding.getName(),
						Bidding.getFinalPrice(),
						Bidding.getIntialPrice()
				});
			}
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		table.setModel(model/*
						 * new DefaultTableModel( new Object[][] {
						 * 
						 * }, new String[] { "Name", "Intial Price", "Final Price" } ) { Class[]
						 * columnTypes = new Class[] { String.class, String.class, String.class };
						 * public Class getColumnClass(int columnIndex) { return
						 * columnTypes[columnIndex]; } }
						 */);
//		table.getColumnModel().getColumn(0).setPreferredWidth(150);
//		table.getColumnModel().getColumn(0).setMinWidth(100);
//		table.getColumnModel().getColumn(0).setMaxWidth(151);
//		table.getColumnModel().getColumn(1).setPreferredWidth(100);
//		table.getColumnModel().getColumn(1).setMinWidth(50);
//		table.getColumnModel().getColumn(1).setMaxWidth(101);
//		table.getColumnModel().getColumn(2).setPreferredWidth(100);
//		table.getColumnModel().getColumn(2).setMinWidth(50);
//		table.getColumnModel().getColumn(2).setMaxWidth(101);
		
		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				AdminProductAdd productAdd = new AdminProductAdd();
				productAdd.ProductAdd();
				dispose();
			}
		});
		btnAdd.setBounds(335, 70, 89, 34);
		contentPane.add(btnAdd);
		
		JButton btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				AdminProductUpdate update = new AdminProductUpdate();
				update.ProductUpdate();
				dispose();
			}
		});
		btnUpdate.setBounds(335, 115, 89, 34);
		contentPane.add(btnUpdate);
		
		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				AdminProductDelete delete = new AdminProductDelete();
				delete.ProductDelete();
				dispose();
			}
		});
		btnDelete.setBounds(335, 160, 89, 34);
		contentPane.add(btnDelete);
		
		JButton button = new JButton("<< Back");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				AdminPanel adminPanel = new AdminPanel();
				adminPanel.Panel();
				dispose();
			}
		});
		button.setBounds(335, 205, 89, 34);
		contentPane.add(button);
		
		JLabel label = new JLabel("@ Admin");
		label.setBounds(10, 38, 73, 21);
		contentPane.add(label);
	}
}
