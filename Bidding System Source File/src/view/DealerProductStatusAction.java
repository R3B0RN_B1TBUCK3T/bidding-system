package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.BiddingController;
import controller.DealerController;
import model.bidding;
import model.dealer;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;

public class DealerProductStatusAction extends JFrame {

	private JPanel contentPane;
	private JTextField txtProduct;
	private JComboBox cBox_Status;
	

	/**
	 * Launch the application.
	 */
	public void StatusAction(String name) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DealerProductStatusAction frame = new DealerProductStatusAction(name);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DealerProductStatusAction(String name) {
		setTitle("Status");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 242);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblDeleteDealer = new JLabel("Product Status");
		lblDeleteDealer.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblDeleteDealer.setBounds(10, 11, 202, 29);
		contentPane.add(lblDeleteDealer);
		
		JLabel lblName = new JLabel("Product");
		lblName.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblName.setBounds(10, 69, 74, 14);
		contentPane.add(lblName);
		
		JLabel label = new JLabel(":");
		label.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label.setBounds(94, 69, 12, 14);
		contentPane.add(label);
		
		txtProduct = new JTextField();
		txtProduct.setBounds(104, 68, 297, 20);
		contentPane.add(txtProduct);
		txtProduct.setColumns(10);
		
		JButton btnDelete = new JButton("OK");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String prodName = txtProduct.getText();
				String status = (String) cBox_Status.getSelectedItem();
				
				bidding Bidding = new bidding();
				Bidding.setName(prodName);
				Bidding.setStatus(status);
				
				BiddingController controller = new BiddingController();
				DealerStatusSuccess success = new DealerStatusSuccess(name);
				
				try {
					controller.updateStatus(Bidding);
					success.DealerSuccess(name);
					dispose();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				dispose();
				
				
			}
		});
		btnDelete.setBounds(104, 158, 89, 34);
		contentPane.add(btnDelete);
		
		JLabel label_1 = new JLabel(":");
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_1.setBounds(94, 113, 12, 14);
		contentPane.add(label_1);
		
		JLabel lblStatus = new JLabel("Status");
		lblStatus.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblStatus.setBounds(10, 113, 74, 14);
		contentPane.add(lblStatus);
		
		cBox_Status = new JComboBox();
		cBox_Status.addItem("In Process");
		cBox_Status.addItem("Successful");
		cBox_Status.setSelectedItem(null);
		cBox_Status.setBounds(104, 112, 297, 20);
		contentPane.add(cBox_Status);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				DealerProductStatus dealerProductStatus = new DealerProductStatus(name);
				dealerProductStatus.ProductStatus(name);
				dispose();
			}
		});
		btnBack.setBounds(220, 158, 89, 34);
		contentPane.add(btnBack);
	}
}
