package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.DealerController;
import model.dealer;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AdminDealerDelete extends JFrame {

	private JPanel contentPane;
	private JTextField txtName;

	/**
	 * Launch the application.
	 */
	public void DealerDelete() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdminDealerDelete frame = new AdminDealerDelete();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AdminDealerDelete() {
		setTitle("Delete Dealer");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 215);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblDeleteDealer = new JLabel("Delete Dealer");
		lblDeleteDealer.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblDeleteDealer.setBounds(10, 11, 151, 29);
		contentPane.add(lblDeleteDealer);
		
		JLabel lblName = new JLabel("Name");
		lblName.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblName.setBounds(10, 74, 46, 14);
		contentPane.add(lblName);
		
		JLabel label = new JLabel(":");
		label.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label.setBounds(93, 74, 12, 14);
		contentPane.add(label);
		
		txtName = new JTextField();
		txtName.setBounds(108, 73, 297, 20);
		contentPane.add(txtName);
		txtName.setColumns(10);
		
		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				dealer Dealer = new dealer();
				String name = txtName.getText();
				Dealer.setfName(name);
				DealerController dealerController = new DealerController();
				AdminDealerSuccess dealerSuccess = new AdminDealerSuccess();
				
				try {
					dealerController.deleteDealer(Dealer);
					dealerSuccess.DealerSuccess();
					dispose();
					//txtName.setText("");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnDelete.setBounds(108, 127, 89, 34);
		contentPane.add(btnDelete);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				AdminDealerPanel dealerPanel = new AdminDealerPanel();
				dealerPanel.DealerPanel();
				dispose();
			}
		});
		btnBack.setBounds(216, 127, 89, 34);
		contentPane.add(btnBack);
		
		JLabel label_1 = new JLabel("@ Admin");
		label_1.setBounds(10, 39, 73, 21);
		contentPane.add(label_1);
	}
}
