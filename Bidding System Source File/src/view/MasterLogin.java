package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.AdminController;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * @author Reborn
 *
 */
public class MasterLogin extends JFrame {

	private JPanel contentPane;
	private JTextField txtUsr;
	private JTextField txtPwd;
	private JButton btnOk;
	private JButton btnSignUp;
	private JLabel lblUtemBidding;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MasterLogin frame = new MasterLogin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public static void Login() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MasterLogin frame = new MasterLogin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MasterLogin() {
		setTitle("Login Panel");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 364, 261);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblLogin = new JLabel("Login ");
		lblLogin.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblLogin.setBounds(10, 11, 64, 29);
		contentPane.add(lblLogin);
		
		JLabel lblUsername = new JLabel("Username");
		lblUsername.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblUsername.setBounds(10, 92, 83, 20);
		contentPane.add(lblUsername);
		
		JLabel label = new JLabel(":");
		label.setFont(new Font("Tahoma", Font.PLAIN, 16));
		label.setBounds(103, 92, 15, 20);
		contentPane.add(label);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblPassword.setBounds(10, 123, 83, 20);
		contentPane.add(lblPassword);
		
		JLabel label_2 = new JLabel(":");
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 16));
		label_2.setBounds(103, 123, 15, 20);
		contentPane.add(label_2);
		
		txtUsr = new JTextField();
		txtUsr.setBounds(113, 92, 213, 22);
		contentPane.add(txtUsr);
		txtUsr.setColumns(10);
		
		txtPwd = new JTextField();
		txtPwd.setColumns(10);
		txtPwd.setBounds(113, 125, 213, 22);
		contentPane.add(txtPwd);
		
		btnOk = new JButton("Login");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String usr = txtUsr.getText();
				String pwd = txtPwd.getText();
				
				AdminController controller = new AdminController();
				try {
					
					if (controller.loginMaster(usr, pwd)) {
						
						
					} else {
						
						JFrame frame = new JFrame();
						JOptionPane.showMessageDialog( frame ,
							    "Unauthorized Access",
							    "Inane error",
							    JOptionPane.ERROR_MESSAGE);
						MasterLogin.Login();
						dispose();
					}
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				dispose();
			}
		});
		btnOk.setBounds(113, 166, 89, 23);
		contentPane.add(btnOk);
		
		btnSignUp = new JButton("Register");
		btnSignUp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MasterRegister.Regsiter();
				dispose();
			}
		});
		btnSignUp.setBounds(212, 166, 89, 23);
		contentPane.add(btnSignUp);
		
		lblUtemBidding = new JLabel("@ UTeM Bidding System");
		lblUtemBidding.setBounds(10, 44, 243, 29);
		contentPane.add(lblUtemBidding);
	}
}
