package model;

import java.util.ArrayList;

public class admin {

	private int id;
	private String fName;
	private String password;
	private String email;
	private ArrayList<dealer> dealers;
	private ArrayList<customer> customers;
	private ArrayList<product> products;
	
	public admin() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public ArrayList<dealer> getDealers() {
		return dealers;
	}

	public void setDealers(ArrayList<dealer> dealers) {
		this.dealers = dealers;
	}

	public ArrayList<customer> getCustomers() {
		return customers;
	}

	public void setCustomers(ArrayList<customer> customers) {
		this.customers = customers;
	}

	public ArrayList<product> getProducts() {
		return products;
	}

	public void setProducts(ArrayList<product> products) {
		this.products = products;
	}
}
