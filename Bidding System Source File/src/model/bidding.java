package model;

public class bidding extends product {
	
	private String intialPrice;
	private String finalPrice;
	private String startTime;
	private String endTime;
	private String status;
	
	public bidding() {
		super();
	}

	public String getIntialPrice() {
		return intialPrice;
	}

	public String getFinalPrice() {
		return finalPrice;
	}

	public String getStartTime() {
		return startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setIntialPrice(String intialPrice) {
		this.intialPrice = intialPrice;
	}

	public void setFinalPrice(String finalPrice) {
		this.finalPrice = finalPrice;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
